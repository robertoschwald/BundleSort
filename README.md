### Functionality:

- This plugin adds a menu option to sort Properties bundles in alphabetic order
- All files in a bundle will be sorted (e.g. Resources.properties, Resources_fr.properties, Resources_en_us.properties)
- Alphabetical order is case sensitive: upper-case before lower-case

### How to use:

Open a .properties file in the editor, either in Text or Resource Bundle editor. The menu Code will contain an option "Sort properties".