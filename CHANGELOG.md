## 1.2 2020-06-10

- ***Added*** integration with Git4Idea plugin: A new option to sort resources is now present in the Git "Commit Changes" window

- ***Fixed*** an issue where multi-line support would fail on lines with property-like format


## 1.1 2020-05-09

- ***Added*** support for multi-line resources
```
multiline-resource = This\
is\
multiline
```

- ***Added*** support for most cases defined in https://docs.oracle.com/javase/10/docs/api/java/util/Properties.html#load(java.io.Reader)
  
    Specifically:
     - ! as comment marker
     - : or whitespace as key value separator
     - Escaped separators in key or value

- ***Added*** line number and content for clarity in error message

- ***Changed*** minimal supported version to 2019.1 (Android Studio 3.5)

- ***Changed*** testing framework to Kotest
