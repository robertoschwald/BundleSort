package ch.namlin.actions

import com.intellij.testFramework.fixtures.LightPlatformCodeInsightFixtureTestCase
import org.junit.Assert

class SortPropertiesActionTest : LightPlatformCodeInsightFixtureTestCase() {

    fun testGetBundle() {
        val file = myFixture.copyDirectoryToProject("Input", ".")
        val action = SortPropertiesAction()
        val bundle = action.getBundle(file.findChild("Resources.properties") ?: throw IllegalStateException())
        Assert.assertEquals(2, bundle.size)
    }

    fun testSort() {
        val file = myFixture.configureByFile("Input/Resources.properties")
        SortPropertiesAction().sortFile(file.virtualFile ?: throw IllegalStateException())
        myFixture.checkResultByFile("Results/Resources.properties")
    }

    fun testIdempotency() {
        val file = myFixture.configureByFile("Input/Idempotent.properties")
        SortPropertiesAction().sortFile(file.virtualFile ?: throw java.lang.IllegalStateException())
        myFixture.checkResultByFile("Results/Idempotent.properties")
    }

    override fun getTestDataPath(): String = "testData"

}
