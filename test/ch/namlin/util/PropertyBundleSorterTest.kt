package ch.namlin.util

import ch.namlin.actions.BundleSortException
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FreeSpec
import io.kotest.data.forAll
import io.kotest.data.row
import io.kotest.matchers.shouldBe

class PropertyBundleSorterTest : FreeSpec({

    "Check that sort handles" - {
        forAll(
                row("Spaces around separator", "aa=aa\nbb  =  bb", "aa = aa\nbb = bb"),

                row("Colon separator", "bB : bB\naA : aA", "aA = aA\nbB = bB"),

                row("Whitespace separator", "bB  bB\naA  aA", "aA = aA\nbB = bB"),

                row("Escaped separator in key", "b\\=\\:\\ B = bB\naA = aA", "aA = aA\nb\\=\\:\\ B = bB"),

                row("Escaped separator in value", "bB = b\\=\\:\\ B\naA = aA", "aA = aA\nbB = b\\=\\:\\ B"),

                row("Empty Line", "bb = bb\n\naa = aa\n\n", "aa = aa\nbb = bb\n"),

                row("Trailing line separator", "aa = aa\n", "aa = aa\n"),

                row("No trailing line separator", "aa = aa", "aa = aa"),

                row("Multiline Comment", "#comment1\n#comment2\nbb = bb\naa = aa", "aa = aa\n#comment1\n#comment2\nbb = bb"),

                row("Comments using !", "bB = bB\n!comment1\naA = aA", "!comment1\naA = aA\nbB = bB"),

                row("End Comment", "aa = aa\n#comment1\n#comment2", "aa = aa\n#comment1\n#comment2"),

                row("Comment symbol in key", "b#B = bB\naA = aA", "aA = aA\nb#B = bB"),

                row("Comment symbol in value", "bB = b#B\naA = aA", "aA = aA\nbB = b#B"),

                row("Trailing Separator", "aa = aa==", "aa = aa=="),

                row("Case Sensitivity", "bB = bB\nba = ba\nbb = bb\nBa = Ba\nBb = Bb\nbA = bA", "Ba = Ba\nBb = Bb\nbA = bA\nbB = bB\nba = ba\nbb = bb"),

                row("Special characters", "aA = aA\na_a = a_a", "aA = aA\na_a = a_a"),

                row("Multiline resource", "bB = bB \\\n bb\\\ncc\naA = aA", "aA = aA\nbB = bB \\\nbb\\\ncc"),

                row("Multiline resource with multiple backslash", "bB = BB\\\\\\\nbb\naA = aA", "aA = aA\nbB = BB\\\\\\\nbb"),

                row("Multiline resource with resource-like second line", "cC = cC\\\nbB = bB\\\ndD\naA = aA", "aA = aA\ncC = cC\\\nbB = bB\\\ndD")

        ) { description, stringToSort, expectedResult ->
            description {
                PropertyBundleSorter().sortAndConvertString(stringToSort, "\n") shouldBe expectedResult
            }
        }

        "CRLF line ending" {
            PropertyBundleSorter().sortAndConvertString("bB = bB\r\naA = aA\r\n", "\r\n") shouldBe "aA = aA\r\nbB = bB\r\n"
        }
    }

    "Sort should fail on" - {
        "Line without equal sign" {
            expectBundleSortException("line1=line1\nline2", 1, "line2")
        }

        "Multiline property without backslash" {
            expectBundleSortException("line1=line1\\\nline2\nline3\\\nline4=line4", 2, "line3\\")
        }

        "Multiline property with even number backslashes" {
            expectBundleSortException("line1=line1\\\\\nline2\n\nline4=line4", 1, "line2")
        }

        "Multiline Resource With Comment" {
            expectBundleSortException("bB = bB\\\n#bb\ncc\naA = aA", 2, "cc")
        }
    }


})

private fun expectBundleSortException(stringToSort: String, expectedIndex: Int, expectedProperty: String) {
    val exception = shouldThrow<BundleSortException> {
        PropertyBundleSorter().sortAndConvertString(stringToSort, "\n")
    }
    exception.index shouldBe expectedIndex
    exception.invalidProperty shouldBe expectedProperty
}