package ch.namlin.extensions

import ch.namlin.actions.BundleSortException
import ch.namlin.util.PropertyBundleSorter
import com.intellij.CommonBundle
import com.intellij.ide.util.PropertiesComponent
import com.intellij.openapi.ui.Messages
import com.intellij.openapi.vcs.CheckinProjectPanel
import com.intellij.openapi.vcs.changes.CommitExecutor
import com.intellij.openapi.vcs.changes.ui.BooleanCommitOption
import com.intellij.openapi.vcs.checkin.CheckinHandler
import com.intellij.openapi.vcs.checkin.VcsCheckinHandlerFactory
import com.intellij.openapi.vcs.ui.RefreshableOnComponent
import com.intellij.openapi.vfs.VfsUtil
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.util.PairConsumer
import com.intellij.util.ui.UIUtil
import git4idea.GitVcs
import org.jetbrains.annotations.Nullable
import java.io.File
import java.util.function.Consumer

class ResourcesCheckinHandlerFactory : VcsCheckinHandlerFactory(GitVcs.getKey()) {
	override fun createVcsHandler(projectPanel: CheckinProjectPanel): CheckinHandler {
		return ResourcesCheckinHandler(projectPanel)
	}


	class ResourcesCheckinHandler(private val panel: CheckinProjectPanel) : CheckinHandler() {
		private val myPanel: CheckinProjectPanel = panel
		private var sortResources
			get() = PropertiesComponent.getInstance(panel.project).getBoolean("ch.namlin.jiragit.sortResources", false)
			set(value) = PropertiesComponent.getInstance(panel.project).setValue("ch.namlin.jiragit.sortResources", value)
		private val propertyBundleSorter = PropertyBundleSorter()

		@Nullable
		override fun getBeforeCheckinConfigurationPanel(): RefreshableOnComponent {
			return BooleanCommitOption(myPanel, "Sort resources", true, { sortResources},
					Consumer { value: Boolean -> sortResources = value })
		}

		override fun beforeCheckin(executor: CommitExecutor?, additionalDataConsumer: PairConsumer<Any, Any>?): ReturnResult {

			if (!sortResources) return ReturnResult.COMMIT

			val failedFiles = tryAndSortFiles()
			if (failedFiles.isNotEmpty()) {
				val commitButtonText = if (executor != null) executor.actionText else panel.commitActionName
				val answer = Messages.showYesNoDialog(panel.project, "Some resources failed to be sorted. Would you like to commit anyway?",
						"Sort resources", commitButtonText, CommonBundle.getCancelButtonText(), UIUtil.getWarningIcon())
				return if (answer == 1) ReturnResult.CANCEL else ReturnResult.COMMIT
			}
			return ReturnResult.COMMIT
		}

		private fun tryAndSortFiles(): MutableList<File> {
			val failedFiles = mutableListOf<File>()
			for (file in myPanel.files) {
				try {
					val virtualFile = VfsUtil.findFileByIoFile(file, true)
					if (file.exists() && virtualFile != null && virtualFile.fileType.name == "Properties") {
						sortFile(file, virtualFile)
					}
				} catch (bundleSortException: BundleSortException) {
					failedFiles.add(file)
				}
			}
			return failedFiles
		}


		private fun sortFile(resourceFile: File, virtualFile: VirtualFile) {
			val fileText = String(resourceFile.readBytes())
			if (fileText.isNotEmpty()) {
				val separator = virtualFile.detectedLineSeparator ?: return
				val newDocumentContent = propertyBundleSorter.sortAndConvertString(fileText, separator)
				resourceFile.writeBytes(newDocumentContent.toByteArray())
			}
		}
	}
}