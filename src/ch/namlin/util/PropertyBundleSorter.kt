package ch.namlin.util

import ch.namlin.actions.BundleSortException
import com.intellij.util.containers.MultiMap


class PropertyBundleSorter {

    data class ParsedPropertyFile(val properties: MultiMap<String, String>, val commentsForProperties: MultiMap<String, String>)

    @Throws(BundleSortException::class)
    fun sortAndConvertString(originalText: String, lineSeparator: String): String {
        val parsedProperties = convertToMap(originalText)
        val sortedProperties = sortAndConvertProperties(parsedProperties.properties, lineSeparator)
        return mergeComments(sortedProperties, parsedProperties.commentsForProperties, lineSeparator, originalText.endsWith(lineSeparator))
    }

    private fun lineContainsProperty(line: String): Boolean = line.contains("(?<!\\\\)[\\s=:]".toRegex())

    private fun lineIsEmpty(line: String): Boolean = line.isBlank()

    private fun lineIsAComment(line: String): Boolean = line.startsWith("#") or line.startsWith('!')

    private fun lineIsMultiline(line: String): Boolean = (line.takeLastWhile { it == '\\' }.length % 2) == 1

    private fun convertToMap(stringToBeConverted: String): ParsedPropertyFile {
        val result = MultiMap<String, String>()
        val commentsForProperties = MultiMap<String, String>()

        var multiLine = Pair(false, "")

        stringToBeConverted.lines().map(String::trim).forEachIndexed { i, line ->
            if (lineIsAComment(line)) {
                val nextPropertyLine = getNextNonCommentLineName(i, stringToBeConverted.lines()) ?: COMMENTS_AT_THE_END
                commentsForProperties.putValue(nextPropertyLine, line)
                multiLine = Pair(false, "")
            } else if (multiLine.first) {
                result.putValue(multiLine.second, line)
                if (!lineIsMultiline(line))
                    multiLine = Pair(false, "")
            } else if (lineContainsProperty(line)) {
                multiLine = Pair(false, "")
                val keyValuePair = extractKeyValuePair(line)
                if(keyValuePair.size == 2) {
                    val key = keyValuePair[0]
                    val value = keyValuePair[1]
                    result.putValue(key, value)

                    if (lineIsMultiline(line))
                        multiLine = Pair(true, key)
                } else {
                    throw BundleSortException(i, line)
                }
            } else if (!lineIsEmpty(line)) {
                throw BundleSortException(i, line)
            }
        }

        return ParsedPropertyFile(result, commentsForProperties)
    }

    private fun extractKeyValuePair(line: String?): List<String> {
        if (line.isNullOrEmpty()) {
            return emptyList()
        }

        // I couldn't devise a regex to handle both symbol and whitespace separator, so I handle both cases separately
        val regex = if (line.contains("(?<!\\\\)[=:]".toRegex())) {
            // Separator is : or =
            "(?<!\\\\)[=:](?=\\s*\\S)".toRegex()
        } else {
            // Separator is whitespace
            "(?<!\\\\)[\\s](?=\\s*\\S)".toRegex()
        }
        return line.split(regex, 2).map(String::trim)
    }

    private fun sortAndConvertProperties(properties: MultiMap<String, String>, lineSeparator: String): String {
        val result = StringBuilder()

        for (key in properties.keySet().toSortedSet()) {
            result.append(key).append(" = ")
            properties[key].forEach { result.append(it).append(lineSeparator) }
        }

        return result.toString().trim(*lineSeparator.toCharArray())
    }

    private fun mergeComments(
            sortedPropertyText: String,
            commentsForProperties: MultiMap<String, String>,
            lineSeparator: String,
            hasTrailingLineSeparator: Boolean
    ): String {
        val result = StringBuilder()

        for (line in sortedPropertyText.lines()) {
            val propertyName = extractKeyValuePair(line).getOrNull(0)
            appendCommentIfExists(commentsForProperties, propertyName, result, lineSeparator)
            result.append(line).append(lineSeparator)
        }

        if (commentsForProperties.containsKey(COMMENTS_AT_THE_END)) {
            val endComments = commentsForProperties[COMMENTS_AT_THE_END].joinToString(lineSeparator)
            result.append(endComments)
        }

        return checkTrailingLineSeparator(hasTrailingLineSeparator, result.toString(), lineSeparator)
    }

    private fun checkTrailingLineSeparator(hasTrailingLineSeparator: Boolean, resultString: String, lineSeparator: String): String{
        if (!hasTrailingLineSeparator && resultString.endsWith(lineSeparator))
            return resultString.trim(*lineSeparator.toCharArray())
        else if (hasTrailingLineSeparator && !resultString.endsWith(lineSeparator))
            return resultString + lineSeparator
        return resultString
    }

    private fun appendCommentIfExists(commentsForProperties: MultiMap<String, String>, propertyName: String?, result: StringBuilder, lineSeparator: String) {
        if (commentsForProperties.containsKey(propertyName))
            result.append(commentsForProperties[propertyName].joinToString(lineSeparator)).append(lineSeparator)
    }

    private fun getNextNonCommentLineName(lineToStart: Int, allLines: List<String>): String? {
        val nextNonCommentLine = allLines.slice(lineToStart until allLines.size)
                .find { it.isNotBlank() && !lineIsAComment(it) }
        return extractKeyValuePair(nextNonCommentLine).getOrNull(0)

    }

    companion object {
        const val COMMENTS_AT_THE_END = "PropertyResourcesSorterEOFComments"
    }
}
