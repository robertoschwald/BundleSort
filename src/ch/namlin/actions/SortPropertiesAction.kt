package ch.namlin.actions

import ch.namlin.util.PropertyBundleSorter
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.ui.Messages
import com.intellij.openapi.vfs.VirtualFile

class SortPropertiesAction : AnAction("Sort properties") {

    private val stringPropertyConverter = PropertyBundleSorter()

    override fun actionPerformed(event: AnActionEvent) {
        val virtualFile = event.getData(CommonDataKeys.VIRTUAL_FILE)

        if (virtualFile != null && isResourceBundle(virtualFile)) {
            try {
                val bundle = getBundle(virtualFile)
                for (resourceFile in bundle) {
                    sortFile(resourceFile)
                }
                say("Properties were sorted.")
            } catch (bundleSortException: BundleSortException) {
                warn("Line ${bundleSortException.index + 1} contains non property styled text. File was not changed.\n\n"
                        + "Affected line: \"${bundleSortException.invalidProperty}\"")
            }

        } else {
            say("Please select an editor window with a properties file.")
        }
    }

    override fun update(event: AnActionEvent) {
        val virtualFile = event.getData(CommonDataKeys.VIRTUAL_FILE) ?: return
        event.presentation.isEnabledAndVisible = isResourceBundle(virtualFile)
    }

    private fun isResourceBundle(virtualFile: VirtualFile) =
            virtualFile.fileType.name == "Properties" || virtualFile.fileType.name == "ResourceBundle"

    internal fun sortFile(resourceFile: VirtualFile) {
        val fileText = String(resourceFile.contentsToByteArray())
        if (!fileText.isEmpty()) {
            val separator = resourceFile.detectedLineSeparator ?: return
            val newDocumentContent = stringPropertyConverter.sortAndConvertString(fileText, separator)

            ApplicationManager.getApplication().runWriteAction {
                resourceFile.setBinaryContent(newDocumentContent.toByteArray())
            }
        }
    }

    internal fun getBundle(virtualFile: VirtualFile): List<VirtualFile> {
        val folder = virtualFile.canonicalFile!!.parent
        val fileName = virtualFile.canonicalFile!!.nameWithoutExtension
        val bundleName = getBundleName(fileName)
        return getBundleFiles(bundleName, folder)
    }

    private fun getBundleFiles(bundleName: String, folder: VirtualFile): List<VirtualFile> {
        val thisNamePattern = Regex("${bundleName}_?(\\w*)")
        return folder.children.filter { child ->
            child.nameWithoutExtension.matches(thisNamePattern) && child.extension == "properties"
        }
    }

    private fun getBundleName(fileName: String): String {
        val namePattern = Regex("""(.*?)_(\w*)""")
        return namePattern.find(fileName)?.groupValues?.get(1) ?: fileName
    }

    private fun say(text: String) {
        Messages.showMessageDialog(text, "Property Sorter Info", Messages.getInformationIcon())
    }

    private fun warn(text: String) {
        Messages.showWarningDialog(text, "Property Sorter Error")
    }

}

class BundleSortException(val index: Int, val invalidProperty: String) : RuntimeException()
